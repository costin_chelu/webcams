﻿COSTIN CHELU
an III ID

email: costin.chelu@gmail.com

  

***Proiect pentru tehnologii web***

  

Aplicația afișează seturi de regiuni și localități din aceste regiuni (județe). În această implementare, am folosit id-ul webcam-urilor pentru afișarea stream-urilor video din respectivele localități, furnizate de API-ul Webcams Travel

  

*React*


1. crearea repository pe bitbucket
2. git init
3. git remote add origin https://costin_chelu@bitbucket.org/costin_chelu/webcams.git
4. npm init
5. npm install react
6. npx create-react-app webcams
7. npm install --save-dev tachyons@4.10.0 (am preferat pachetul de componente tachyons)
8. npm i fbemitter
9. Componente si store-uri

  

*API*


Pentru testarea endpoint-urilor server-ului precum si al API-ului webcams-travel am folosit Postman 7.2

 - creare schema "webcams" cu mysql
 -  creare fisier server.js 
 - npm init 
 - npm install express --save
 -  npm install cors 
 - npm install --save sequelize  
 - npm install --save mysql2 
 - npm i fbemitter npm i body-parser  
 - completare sequelize și endpoints integrarea cu serviciul extern webcams-travel (rapid.api)

  
Pentru funcționarea corectă cu API-ul Webcams Travel, atributul Webcam al respectivei intrări (localitate) trebuie să fie un id valid. 
Exemple de id valid:

    1539770011
    1263629371
    1530301468
    1297606837
    1547591341
    1214473250
    1567325940
    1520257172
    1261418327
    1520758803

  
Toate celelalte atribute ale regiunilor sau orașelor pot cuprinde orice format, în afară de atributele latitudine și longitudine care trebuie să fie în format nr real (ex: 45.48983)
